
public class Main {
	
	private static final int N_ACCOUNTS = 20;

	public static void main(String[] args) throws NoAccException {
		
		Bank b = new Bank(10);

		for (int i = 0; i < N_ACCOUNTS; i++)
			b.newAccount(i);

		for (int i = 0; i < N_ACCOUNTS; i++)
			b.deposit(i, i * 1000);
		
		b.setclimit(0, 1000);
		
		for (int i = 0; i < N_ACCOUNTS; i++)
			b.withdraw(i, 100);

		b.removeAccount(3);
		b.newAccount(3);
		b.transfer(6, 3, 3000);
		
		for (int i = 0; i < N_ACCOUNTS; i++)
			System.out.println(b.getBalance(i) + " in account " + i);
		
	}
}
