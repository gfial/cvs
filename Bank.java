/*@
predicate AccountP(unit a, BankAccount c; int n) = 
c != null &*& AccountInv(c,n,?m);

predicate BankInv(Bank bk; int n, int m) = 
     bk.nelems |-> n &*& 
     bk.capacity |-> m &*& 
     m > 0 &*&
     bk.store |-> ?a &*&
     a != null &*&
     a.length == m &*&
     0 <= n &*& n<=m &*& 
     array_slice_deep(a, 0, n, AccountP, unit,_,?bs) &*&
     array_slice(a, n, m,?rest) &*& 
     all_eq(rest, null) == true ;

predicate IsntAccount(int code, BankAccount c; int id) = 
    c != null &*& c.accountid |-> id &*& id != code;

predicate NoAccount(Bank bk, int id) =
	bk.store |-> ?a &*&
	bk.nelems |-> ?n &*&
	bk.capacity |-> ?m &*& 
	array_slice_deep(a, 0, n, IsntAccount, id, _, _) &*&
	array_slice_deep(a, 0, n, AccountP, unit,_,?bs) &*&
    array_slice(a, n, m,?rest) &*& 
    all_eq(rest, null) == true;
@*/

class Bank {

	BankAccount store[];
	int nelems;
	int capacity;

	public Bank(int siz)
	//@ requires siz>0;
	//@ ensures BankInv(this,0,siz);
	{
		nelems = 0;
		capacity = siz;
		store = new BankAccount[siz];
	}

	public void newAccount(int code)
	//@ requires BankInv(this,?n,?m) &*& n < m &*& ValidId(code) == true &*& NoAccount(this, code);
	//@ ensures m == n+1? BankInv(this,n+1,2*m) : BankInv(this,n+1,m);
	{
		int i = 0;
		
		if(capacity == nelems + 1){
			extendstore();
			//@ open BankInv(this,n,2*m);
		}

		//@ open NoAccount(this, code);
		while (i < nelems)
		//@ invariant BankInv(this,n,m) &*& 0 <= i &*& i <= n;
		{
			if (store[i].getcode() == code) {
				return;
			}
			i = i + 1;
		}

		//@ array_slice_split(store, nelems,nelems+1);
		BankAccount c = new BankAccount(code);
		store[nelems] = c;
		//@ array_slice_deep_close(store, nelems, AccountP, unit);
		nelems = nelems + 1;
	}

	/* throws exception if acc_code does not exist */
	public int getBalance(int code) throws NoAccException
	//@ ensures BankInv(this,_,_);
	//@ requires BankInv(this,?n,?m) &*& ValidId(code) == true;
	//@ ensures BankInv(this,n,m);
	{
		int i = 0;

		//@ open BankInv(this,n,m);
		while (i < nelems)
		//@ invariant BankInv(this,n,m) &*& 0 <= i &*& i <= n;
		{
			if (store[i].getcode() == code) {
				return store[i].getbalance();
			}
			i = i + 1;
		}
		
		throw new NoAccException();
	}

	public int removeAccount(int code)
	//@ requires BankInv(this,?n,?m);
	//@ ensures result == 0 ? BankInv(this,n-1,m) : BankInv(this,n,m);
	{
		int i = 0;
		//@ open BankInv(this,n,m);
		while (i < nelems)
		//@ invariant BankInv(this,n,m) &*& 0 <= i &*& i <= n;
		{
			if (store[i].getcode() == code) {
				if (i < nelems - 1) {
					store[i] = store[nelems - 1];
				}
				nelems = nelems - 1;
				store[nelems] = null;
				return 0;
			}
			i = i + 1;
		}
		return -1;
	}
	
	public int deposit(int code, int val)
	//@ requires BankInv(this, ?n, ?m) &*& val > 0 &*& ValidId(code) == true;
	//@ ensures BankInv(this, n, m); 
	{
		int i = 0;
		//@ open BankInv(this,n,m);
		while (i < nelems)
		//@ invariant BankInv(this,n,m) &*& 0 <= i &*& i <= n;
		{
			if (store[i].getcode() == code) {
				store[i].deposit(val);
				return store[i].getbalance();
			}
			i = i + 1;
		}
		return -1;
	}
	
	public int withdraw(int code, int val)
	//@ requires BankInv(this, ?n, ?m) &*& val > 0 &*& ValidId(code) == true;
	//@ ensures BankInv(this, n, m); 
	{
		int i = 0;
		//@ open BankInv(this,n,m);
		while (i < nelems)
		//@ invariant BankInv(this,n,m) &*& 0 <= i &*& i <= n;
		{
			if (store[i].getcode() == code) {
				if(store[i].getbalance() + store[i].getclimit() < val)
					return -1;
				store[i].withdraw(val);
				return store[i].getbalance();
			}
			i = i + 1;
		}
		return -1;
	}
	
	public int setclimit(int code, int cl)
	//@ requires BankInv(this, ?n, ?m) &*& cl > 0 &*& ValidId(code) == true;
	//@ ensures BankInv(this, n, m); 
	{
		int i = 0;
		//@ open BankInv(this,n,m);
		while (i < nelems)
		//@ invariant BankInv(this,n,m) &*& 0 <= i &*& i <= n;
		{
			if (store[i].getcode() == code) {
				if(store[i].getbalance() + cl < 0)
					return -1;
				store[i].setclimit(cl);
				return store[i].getclimit();
			}
			i = i + 1;
		}
		return -1;
	}
	
	/* throws exception if acc_code does not exist */
	public int getclimit(int code) throws NoAccException
	//@ ensures BankInv(this,_,_);
	//@ requires BankInv(this, ?n, ?m) &*& ValidId(code) == true;
	//@ ensures BankInv(this, n, m); 
	{
		int i = 0;
		//@ open BankInv(this,n,m);
		while (i < nelems)
		//@ invariant BankInv(this,n,m) &*& 0 <= i &*& i <= n;
		{
			if (store[i].getcode() == code) {
				return store[i].getclimit();
			}
			i = i + 1;
		}
		
		throw new NoAccException();
	}
	
	public boolean transfer(int acc_code_from, int acc_code_to, int val)
	//@ requires BankInv(this,?n,?m) &*& val > 0;
	//@ ensures BankInv(this,n,m);
	{

		int i = 0;
		//@ open BankInv(this,n,m);
		while (i < nelems)
		//@ invariant BankInv(this,n,m) &*& 0 <= i &*& i <= n;
		{
			if (store[i].getcode() == acc_code_from) {
				int j = 0;
				//@ open BankInv(this,n,m);
				while (j < nelems)
				//@ invariant BankInv(this,n,m) &*& 0 <= j &*& j <= n &*& 0 <= i &*& i <= n;
				{
					if (store[j].getcode() == acc_code_to) {
						//@ close BankInv(this,n,m);
						if (store[i].getbalance() + store[i].getclimit() >= val) {
							store[i].withdraw(val);
							//@ close BankInv(this,n,m);
							store[j].deposit(val);
						}
						return true;
					}
					j = j + 1;
				}
			}
			i = i + 1;
		}

		return false;
	}

	private void extendstore()
	//@ requires BankInv(this,?n,?m);
	//@ ensures BankInv(this,n,2*m);
	{
		int i = 0;
		BankAccount newstore[] = new BankAccount[capacity * 2];
		//@ open BankInv(this,n,m);
		while (i < nelems)
		/*@
		 invariant this.nelems |-> n &*& this.capacity |-> m &*& this.store
		 |-> ?a &*& a.length == m &*& 0 <= n &*& n<=m &*& array_slice(a, 0,
		 i,?r1) &*& all_eq(r1, null) == true &*& array_slice_deep(a, i, n,
		 AccountP, unit,_,?bs) &*& array_slice(a, n, m,?rest) &*& all_eq(rest,
		 null) == true &*& array_slice_deep(newstore, 0, i, AccountP,
		 unit,_,_) &*& array_slice(newstore, i, m*2,?r2) &*& all_eq(r2, null)
		 == true &*& 0 <= i &*& i <= n;
		 @*/
		{
			newstore[i] = store[i];
			store[i] = null;
			i++;
		}
		capacity = 2 * capacity;
		store = newstore;
		return;
	}

}
